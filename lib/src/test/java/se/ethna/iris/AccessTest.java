/*
 * Copyright 2021 Erland Arctaedius
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package se.ethna.iris;

import org.junit.Assert;
import org.junit.Test;

public class AccessTest {
    static abstract class Config {
        abstract int getNumber();
        abstract String getName();
        private double getValue() {
            return 1.23;
        }
    }

    @Test
    public void test1() {
        String[] cmd = "--number 9 --name gubbe".split(" ");
        Config cfg = Iris.get(Config.class, cmd);

        Assert.assertEquals(9, cfg.getNumber());
        Assert.assertEquals("gubbe", cfg.getName());
        Assert.assertEquals(1.23, cfg.getValue(), 0.00001);
    }
}
