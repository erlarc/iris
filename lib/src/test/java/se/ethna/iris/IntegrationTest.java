/*
 * Copyright 2021 Erland Arctaedius
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package se.ethna.iris;

import org.junit.Assert;
import org.junit.Test;

public class IntegrationTest {

    @Test
    public void mainTest1() {
        String[] cmd = "--name hej --number 7".split(" ");
        Config1 cfg = Iris.get(Config1.class, cmd);

        Assert.assertEquals("hej", cfg.getName());
        Assert.assertEquals(7, cfg.getNumber());
        Assert.assertEquals(55, cfg.getValue());
    }

    @Test
    public void mainTest2() {
        String[] cmd = "--name da --number 8".split(" ");
        Config1 cfg = Iris.get(Config1.class, cmd);

        Assert.assertEquals("da", cfg.getName());
        Assert.assertEquals(8, cfg.getNumber());
        Assert.assertEquals(55, cfg.getValue());
    }

    interface Config1 {
        String getName();
        int getNumber();
        default int getValue() {
            return 55;
        }
    }
}
