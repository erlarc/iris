/*
 * Copyright 2021 Erland Arctaedius
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package se.ethna.iris;

import org.junit.Assert;
import org.junit.Test;

public class TypeTest {
    interface Primitive {
        Integer getInteger();
        int getInt();
    }

    @Test
    public void primitiveInts() {
        String[] cmd = "--integer 4 --int -5".split(" ");
        Primitive cfg = Iris.get(Primitive.class, cmd);

        Assert.assertEquals(Integer.valueOf(4), cfg.getInteger());
        Assert.assertEquals(-5, cfg.getInt());
    }

    interface NullInteger {
        Integer getValue();
    }

    @Test
    public void nullInteger() {
        String[] cmd = "--value null".split(" ");
        NullInteger cfg = Iris.get(NullInteger.class, cmd);

        Assert.assertEquals(null, cfg.getValue());
    }
}
