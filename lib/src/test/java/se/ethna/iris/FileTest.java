/*
 * Copyright 2021 Erland Arctaedius
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package se.ethna.iris;

import org.junit.Assert;
import org.junit.Test;

import java.net.URL;

public class FileTest {
    private static final URL file1 = FileTest.class.getClassLoader().getResource("FileTest.1.conf");
    private static final URL file2 = FileTest.class.getClassLoader().getResource("FileTest.2.conf");

    private interface Config {
        String getName();

        int age();

        default String word() {
            return "jaja";
        }
    }

    @Test
    public void test1() {
        Config cfg = Iris.get(Config.class, file1);

        Assert.assertEquals("hej", cfg.getName());
        Assert.assertEquals(5, cfg.age());
        Assert.assertEquals("jaja", cfg.word());
    }

    @Test
    public void test2() {
        Config cfg = Iris.get(Config.class, file2);

        Assert.assertEquals("hej", cfg.getName());
        Assert.assertEquals(5, cfg.age());
        Assert.assertEquals("nejnej", cfg.word());
    }
}
