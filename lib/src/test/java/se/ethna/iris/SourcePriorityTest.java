/*
 * Copyright 2021 Erland Arctaedius
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package se.ethna.iris;

import org.junit.Assert;
import org.junit.Test;
import se.ethna.iris.parse.CommandLineConfigSource;
import se.ethna.iris.parse.InputStreamConfigSource;

import java.io.IOException;
import java.net.URL;

public class SourcePriorityTest {
    // name = hej, age = 5
    private static final URL file1 = FileTest.class.getClassLoader().getResource("SourcePriorityTest.1.conf");
    // age = 7, word = nejnej
    private static final URL file2 = FileTest.class.getClassLoader().getResource("SourcePriorityTest.2.conf");
    private static final String[] cmdLine = "--age 2 --name da".split(" ");

    private interface Config {
        String getName();

        int age();

        default String word() {
            return "jaja";
        }
    }

    @Test
    public void cmdBeforeStream() throws IOException {
        CommandLineConfigSource cmdSrc = new CommandLineConfigSource(cmdLine);
        InputStreamConfigSource fileSrc = new InputStreamConfigSource(file1.openStream());

        Config cfg = Iris.get(Config.class, cmdSrc, fileSrc);

        Assert.assertEquals(2, cfg.age());
        Assert.assertEquals("da", cfg.getName());
        Assert.assertEquals("jaja", cfg.word());
    }

    @Test
    public void streamBeforeCmd() throws IOException {
        CommandLineConfigSource cmdSrc = new CommandLineConfigSource(cmdLine);
        InputStreamConfigSource fileSrc = new InputStreamConfigSource(file1.openStream());

        Config cfg = Iris.get(Config.class, fileSrc, cmdSrc);

        Assert.assertEquals(5, cfg.age());
        Assert.assertEquals("hej", cfg.getName());
        Assert.assertEquals("jaja", cfg.word());
    }

    @Test
    public void firstSourceLackingCfg() throws IOException {
        CommandLineConfigSource cmdSrc = new CommandLineConfigSource("--name da".split(" "));
        InputStreamConfigSource fileSrc = new InputStreamConfigSource(file1.openStream());

        Config cfg = Iris.get(Config.class, cmdSrc, fileSrc);

        Assert.assertEquals(5, cfg.age());
        Assert.assertEquals("da", cfg.getName());
        Assert.assertEquals("jaja", cfg.word());
    }

    @Test
    public void threeSources() throws IOException {
        CommandLineConfigSource cmdSrc = new CommandLineConfigSource("--name da".split(" "));
        InputStreamConfigSource file1Src = new InputStreamConfigSource(file1.openStream());
        InputStreamConfigSource file2Src = new InputStreamConfigSource(file2.openStream());

        Config cfg = Iris.get(Config.class, cmdSrc, file2Src, file1Src);

        Assert.assertEquals(7, cfg.age());
        Assert.assertEquals("da", cfg.getName());
        Assert.assertEquals("nejnej", cfg.word());
    }
}
