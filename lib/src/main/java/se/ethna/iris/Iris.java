/*
 * Copyright 2021 Erland Arctaedius
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package se.ethna.iris;

import se.ethna.iris.parse.ConfigSource;
import se.ethna.iris.parse.InputStreamConfigSource;

import java.io.File;
import java.io.IOException;
import java.net.URL;

public interface Iris {
    static <T> IrisBuilder<T> builder(Class<T> klass) {
        return new IrisBuilder<>(klass);
    }

    static <T> T get(Class<T> klass, String[] commandline) {
        return builder(klass)
                .commandLine(commandline)
                .build();
    }

    static <T> T get(Class<T> klass, File file) {
        return builder(klass)
                .file(file)
                .build();
    }

    static <T> T get(Class<T> klass, URL url) {
        try {
            return builder(klass)
                    .from(new InputStreamConfigSource(url.openStream()))
                    .build();
        } catch (IOException e) {
            throw new RuntimeException("Could not open configuration source " + url, e);
        }
    }

    static <T> T get(Class<T> klass, ConfigSource... sources) {
        IrisBuilder<T> builder = builder(klass);
        for (ConfigSource source : sources) {
            builder = builder.from(source);
        }
        return builder.build();
    }
}
