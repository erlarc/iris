/*
 * Copyright 2021 Erland Arctaedius
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package se.ethna.iris.construct;

import net.bytebuddy.ByteBuddy;
import net.bytebuddy.dynamic.DynamicType;
import net.bytebuddy.dynamic.loading.ClassLoadingStrategy;
import net.bytebuddy.implementation.FixedValue;
import net.bytebuddy.matcher.ElementMatchers;
import se.ethna.iris.parse.ParsedConfig;
import se.ethna.iris.parse.ParsedConfigElement;

import java.lang.invoke.MethodHandles;
import java.lang.reflect.InvocationTargetException;

public class ConfigConstructor<T> {
    private final Class<T> klass;

    public ConfigConstructor(Class<T> klass) {
        this.klass = klass;
    }

    public T construct(ParsedConfig config) {
        // TODO: it should be possible to construct the class only once and then create new instances with different config values
        // the benefit of this is that we might implement equals and hashCode etc.

        DynamicType.Builder<T> builder = new ByteBuddy()
                .subclass(klass);

        builder = addMethods(builder, config);

        try {
            Class<? extends T> implKlass = builder.make()
                    .load(klass.getClassLoader(), ClassLoadingStrategy.UsingLookup.of(MethodHandles.privateLookupIn(klass, MethodHandles.lookup())))
                    .getLoaded();

            return implKlass.getDeclaredConstructor().newInstance();
        } catch (InstantiationException | InvocationTargetException | NoSuchMethodException | IllegalAccessException e) {
            throw new ConstructorException(e);
        }
    }

    private DynamicType.Builder<T> addMethods(DynamicType.Builder<T> builder, ParsedConfig config) {
        for (ParsedConfigElement configElement : config.parsedConfigElements()) {
            if (!configElement.hasValue()) {
                continue;
            }
            Object value = configElement.value();
            builder = builder.method(ElementMatchers.named(configElement.configElement().method().getName()))
                    .intercept(value == null ?
                            FixedValue.nullValue() :
                            FixedValue.value(value)
                    );
        }
        return builder;
    }
}
