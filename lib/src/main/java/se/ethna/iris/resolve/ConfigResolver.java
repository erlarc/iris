/*
 * Copyright 2021 Erland Arctaedius
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package se.ethna.iris.resolve;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;

public class ConfigResolver {
    private static final List<String> IGNORED_PREFIXES = Arrays.asList("get", "is");

    public Config resolve(Class<?> klass) {
        Method[] declaredMethods = klass.getDeclaredMethods();
        ArrayList<ConfigElement> configElements = new ArrayList<>(declaredMethods.length);

        for (Method method : declaredMethods) {
            if (shouldIncludeMethod(method)) {
                configElements.add(parseMethod(method));
            }
        }

        return new Config(configElements);
    }

    private boolean shouldIncludeMethod(Method method) {
        return !method.isBridge() && !method.isSynthetic() && method.getDeclaringClass() != Object.class;
    }

    private ConfigElement parseMethod(Method method) {
        String name = parseConfigName(method);
        ConfigType type = parseConfigType(method);
        boolean isAbstract = Modifier.isAbstract(method.getModifiers());

        return new ConfigElement(type, name, method, isAbstract);
    }

    private String parseConfigName(Method method) {
        String baseName = method.getName();
        return camelCaseToDashes(stripPrefixes(baseName));
    }

    private String stripPrefixes(String name) {
        for (String prefix : IGNORED_PREFIXES) {
            if (name.toLowerCase(Locale.ROOT).startsWith(prefix)) {
                return name.substring(prefix.length());
            }
        }
        return name;
    }

    private String camelCaseToDashes(String name) {
        StringBuilder configName = new StringBuilder();
        for (char c : name.toCharArray()) {
            if (Character.isUpperCase(c)) {
                char charToUse = Character.toLowerCase(c);
                if (configName.isEmpty()) {
                    configName.append(charToUse);
                } else {
                    configName.append("-").append(charToUse);
                }
            } else {
                configName.append(c);
            }
        }
        return configName.toString();
    }

    private ConfigType parseConfigType(Method method) {
        for (ConfigType value : ConfigType.values()) {
            if (method.getReturnType().isAssignableFrom(value.type())) {
                return value;
            }
        }
        String errorMsg = String.format("Method %s in class %s has unsupported return type %s",
                method.getName(),
                method.getDeclaringClass().getName(),
                method.getReturnType().getName());
        throw new IllegalArgumentException(errorMsg);
    }
}
