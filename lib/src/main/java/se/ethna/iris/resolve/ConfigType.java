/*
 * Copyright 2021 Erland Arctaedius
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package se.ethna.iris.resolve;

import java.util.function.Function;

public enum ConfigType {
    String(String.class, Function.identity()),
    Int(int.class, java.lang.Integer::parseInt),
    Integer(java.lang.Integer.class, acceptNull(java.lang.Integer::parseInt)),
    Flt(float.class, java.lang.Float::parseFloat),
    Float(java.lang.Float.class, acceptNull(java.lang.Float::parseFloat)),
    Dbl(double.class, java.lang.Double::parseDouble),
    Double(java.lang.Double.class, acceptNull(java.lang.Double::parseDouble));

    private final Class<?> type;
    private final Function<String, ?> parser;

    <T> ConfigType(Class<T> type, Function<String, ? super T> parser) {
        this.type = type;
        this.parser = parser;
    }

    public Class<?> type() {
        return type;
    }

    public Object parse(String argument) {
        return parser.apply(argument);
    }

    private static <T> Function<String, T> acceptNull(Function<String, T> base) {
        return s -> "null".equals(s) ? null : base.apply(s);
    }
}
