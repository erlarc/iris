/*
 * Copyright 2021 Erland Arctaedius
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package se.ethna.iris;

import se.ethna.iris.construct.ConfigConstructor;
import se.ethna.iris.parse.CommandLineConfigSource;
import se.ethna.iris.parse.ConfigParser;
import se.ethna.iris.parse.ConfigSource;
import se.ethna.iris.parse.FileConfigSource;
import se.ethna.iris.parse.ParsedConfig;
import se.ethna.iris.resolve.Config;
import se.ethna.iris.resolve.ConfigResolver;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class IrisBuilder<T> {
    private final Class<T> klass;
    private final List<ConfigSource> sources;

    public IrisBuilder(Class<T> klass) {
        this(klass, Collections.emptyList());
    }

    /**
     * Ensure {@code sources} is not modified after this call. {@code IrisBuilder} relies on the list being immutable.
     */
    private IrisBuilder(Class<T> klass, List<ConfigSource> sources) {
        if (klass == null) throw new NullPointerException();
        this.klass = klass;
        this.sources = Collections.unmodifiableList(sources);
    }

    public IrisBuilder<T> from(ConfigSource source) {
        if (source == null) throw new NullPointerException();
        ArrayList<ConfigSource> configSources = new ArrayList<>(sources);
        configSources.add(source);
        return new IrisBuilder<>(klass, configSources);
    }

    public IrisBuilder<T> commandLine(String[] commandLine) {
        return from(new CommandLineConfigSource(commandLine));
    }

    public IrisBuilder<T> file(File file) {
        return from(new FileConfigSource(file));
    }

    public IrisBuilder<T> file(String filename) {
        return file(new File(filename));
    }

    public T build() {
        if (sources.size() <= 0) throw new IllegalStateException("No sources configured for configuration");

        ConfigResolver resolver = new ConfigResolver();
        Config config = resolver.resolve(klass);

        ConfigParser configParser = new ConfigParser(config, sources);
        ParsedConfig parsedConfig = configParser.parse();

        ConfigConstructor<T> configConstructor = new ConfigConstructor<>(klass);
        return configConstructor.construct(parsedConfig);
    }
}
