/*
 * Copyright 2021 Erland Arctaedius
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package se.ethna.iris.parse;

import se.ethna.iris.resolve.ConfigElement;

import java.util.Objects;

public class ParsedConfigElement {
    private final ConfigElement configElement;
    private final boolean hasValue;
    private final Object value;

    public ParsedConfigElement(ConfigElement configElement, Object value) {
        this.configElement = configElement;
        this.value = value;
        this.hasValue = true;
    }

    public ParsedConfigElement(ConfigElement configElement) {
        this.configElement = configElement;
        this.value = null;
        this.hasValue = false;
    }

    public ConfigElement configElement() {
        return configElement;
    }

    public Object value() {
        if (!hasValue) {
            throw new IllegalStateException("Can't get value from config without value");
        }
        return value;
    }

    public boolean hasValue() {
        return hasValue;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ParsedConfigElement that = (ParsedConfigElement) o;
        return hasValue == that.hasValue && Objects.equals(configElement, that.configElement) && Objects.equals(value, that.value);
    }

    @Override
    public int hashCode() {
        return Objects.hash(configElement, hasValue, value);
    }
}
