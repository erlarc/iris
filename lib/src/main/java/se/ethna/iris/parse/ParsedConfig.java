/*
 * Copyright 2021 Erland Arctaedius
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package se.ethna.iris.parse;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Objects;

public class ParsedConfig {
    private final List<ParsedConfigElement> parsedConfigElements;

    public ParsedConfig(List<ParsedConfigElement> parsedConfigElements) {
        this.parsedConfigElements = new ArrayList<>(parsedConfigElements);
    }

    public List<ParsedConfigElement> parsedConfigElements() {
        return Collections.unmodifiableList(parsedConfigElements);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ParsedConfig that = (ParsedConfig) o;
        return Objects.equals(parsedConfigElements, that.parsedConfigElements);
    }

    @Override
    public int hashCode() {
        return Objects.hash(parsedConfigElements);
    }
}
