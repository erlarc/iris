/*
 * Copyright 2021 Erland Arctaedius
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package se.ethna.iris.parse;

import se.ethna.iris.resolve.Config;

import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class InputStreamConfigSource extends ConfigSourceBase {
    private final InputStream is;

    public InputStreamConfigSource(InputStream is) {
        this.is = is;
    }

    public Map<String, String> readConfigMap(Config config) {
        Map<String, String> map = new HashMap<>();
        try (Scanner scanner = new Scanner(is)) {
            while (scanner.hasNext()) {
                // TODO support comments using # maybe?
                String line = scanner.nextLine().strip();
                if (line.isEmpty()) continue; // Empty lines are fine
                int firstSpaceIdx = line.indexOf(' ');
                if (firstSpaceIdx == -1)
                    throw new IllegalArgumentException("Malformed config: line \"" + line + "\" is not valid");
                String name = line.substring(0, firstSpaceIdx).strip();
                String value = line.substring(firstSpaceIdx + 1).strip();
                map.put(name, value);
            }
        }
        return map;
    }
}
