/*
 * Copyright 2021 Erland Arctaedius
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package se.ethna.iris.parse;

import se.ethna.iris.resolve.Config;

import java.util.HashMap;
import java.util.Map;

public class CommandLineConfigSource extends ConfigSourceBase {
    private final String[] cmdLine;

    public CommandLineConfigSource(String[] cmdLine) {
        this.cmdLine = cmdLine;
    }

    @Override
    public Map<String, String> readConfigMap(Config cfg) {
        if (cmdLine.length % 2 != 0) {
            throw new IllegalArgumentException("Mismatched number of arguments: not all arguments have a value");
        }
        Map<String, String> map = new HashMap<>(cmdLine.length);

        for (int i = 0; i < cmdLine.length; i += 2) {
            // TODO - should we normalize case here? Should that be an option maybe?
            String maybeName = cmdLine[i];
            if (!maybeName.startsWith("--")) throw new IllegalArgumentException("Not an option: " + maybeName);
            String name = maybeName.substring(2);
            String value = cmdLine[i + 1];
            map.put(name, value);
        }

        return map;
    }
}
