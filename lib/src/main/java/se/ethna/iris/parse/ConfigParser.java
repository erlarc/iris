/*
 * Copyright 2021 Erland Arctaedius
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package se.ethna.iris.parse;

import se.ethna.iris.resolve.Config;
import se.ethna.iris.resolve.ConfigElement;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class ConfigParser {
    private final Config config;
    /**
     * All the sources to be used, in <i>_increasing_</i> priority
     */
    private final List<ConfigSource> sources;

    public ConfigParser(Config config, ConfigSource... sources) {
        this(config, Arrays.asList(sources));
    }

    public ConfigParser(Config config, List<ConfigSource> sources) {
        if (sources == null || sources.size() < 1)
            throw new IllegalArgumentException("bad number of sources, null or 0");
        this.config = config;

        // since Collections.reverse() is in place, copy the list. Also prevents unwanted modification...
        List<ConfigSource> reversedSources = new ArrayList<>(sources);
        Collections.reverse(reversedSources);
        this.sources = reversedSources;
    }

    public ParsedConfig parse() {
        return parseConfigMap(mergeSources());
    }

    private Map<String, String> mergeSources() {
        HashMap<String, String> cfgMap = new HashMap<>();

        for (ConfigSource source : sources) {
            cfgMap.putAll(source.getConfigMap(config));
        }

        return cfgMap;
    }

    private ParsedConfig parseConfigMap(Map<String, String> map) {
        List<ParsedConfigElement> parsedElements = new ArrayList<>(config.configElements().size());
        Set<String> usedElements = new HashSet<>(map.size());

        for (ConfigElement configElement : config.configElements()) {
            String argument = map.get(configElement.name());

            if (configElement.isAbstract() && argument == null) {
                throw new IllegalArgumentException("Missing value for non-optional config " + configElement.name());
            }

            ParsedConfigElement parsedElement;
            if (argument == null) {
                parsedElement = new ParsedConfigElement(configElement);
            } else {
                Object value = configElement.type().parse(argument);
                parsedElement = new ParsedConfigElement(configElement, value);
                usedElements.add(configElement.name());
            }
            parsedElements.add(parsedElement);
        }

        checkNoUnknownElements(usedElements, map);

        return new ParsedConfig(parsedElements);
    }

    /**
     * Take a set of all the elements we actually checked for, and check if there are any in the resolved
     * config map that we didn't use - that ought to be an error.
     */
    private void checkNoUnknownElements(Set<String> usedElements, Map<String, String> configElementMap) {
        if (usedElements.size() < configElementMap.size()) {
            // Okay, we have an error. Find them and provide them, for usability's sake
            HashSet<String> configKeys = new HashSet<>(configElementMap.keySet());
            configKeys.removeAll(usedElements);
            String msg = String.format("Non-recognized config%s found: %s", configKeys.size() > 1 ? "s" : "", configKeys.toString());
            throw new IllegalArgumentException(msg);
        }
    }
}
