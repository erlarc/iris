/*
 * Copyright 2021 Erland Arctaedius
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package se.ethna.iris.parse;

import se.ethna.iris.resolve.Config;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.util.Map;

public class FileConfigSource extends ConfigSourceBase {
    private final File file;

    public FileConfigSource(File file) {
        this.file = file;
    }

    public Map<String, String> readConfigMap(Config config) {
        try (InputStream is = new FileInputStream(file)) {
            return new InputStreamConfigSource(is).getConfigMap(config);
        } catch (Exception e) {
            throw new RuntimeException(String.format("Could not read config file %s", file.getAbsolutePath()), e);
        }
    }
}
